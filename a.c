/*
relationship between profit and ticket price:
ticket price = x
participants = y

y= -4x + 180

income = ticket price*participants
income = x*y
income = x*(-4*x + 180)
income = -4*x*x + 180*x

expenses = 3*participants + 500
expenses = 3*y + 500
expenses = 3(-4*x + 180) + 500
expenses = -12*x + 1040

profit = income - expenses
profit = -4*x*x + 180*x - (-12*x + 1040)
profit = -4*x*x +192*x -1040

when profit is maximum derivative of profit is zero
derivative of profit with respect to x is = -8*x + 192
-8*x + 192 = 0
x = 192/8
x=24
here x is the maximum ticket value for the maximum profit
*/

#include<stdio.h>

float profit(float ticketPrice)
{
      return -4.0*ticketPrice*ticketPrice + 192.0*ticketPrice - 1040.0;
}

int participants(float ticketPrice)
{
      return -4 *(int)ticketPrice +180;
}

float maxTicket()
{
// ticket price which gives the maximum profit
      return 192.0/8.0;
}

int main()
{
float ticketPrice;
ticketPrice=maxTicket();

printf("Most profitable ticket price: Rs. %0.2f\n",ticketPrice);
printf("Amount of participants: %d \n",participants(ticketPrice));
printf("Profit: Rs. %0.2f\n",profit(ticketPrice));

return 0;
}
